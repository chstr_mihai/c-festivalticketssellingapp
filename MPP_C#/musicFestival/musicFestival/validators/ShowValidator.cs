﻿using musicFestival.domain;
using musicFestival.exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace musicFestival.validators
{
    class ShowValidator : IValidator<Show>
    {
        public void Validate(Show entity)
        {
            string error = "";

            if (entity.Place == "")
            {
                error += "Location invalid";
            }

            if (entity.Artist == "")
            {
                error += "Artist invalid";
            }

            if (entity.SoldSeats < 0)
            {
                error += "Tickets sold cannot be less than 0";
            }

            if (entity.AvailableSeats < 0)
            {
                error += "Tickets available cannot be less than 0";
            }

            if (error != "")
            {
                throw new ValidatorException(error);
            }
        }
    }
}

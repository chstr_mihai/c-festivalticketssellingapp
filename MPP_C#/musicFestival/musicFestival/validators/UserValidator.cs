﻿using musicFestival.domain;
using musicFestival.exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace musicFestival.validators
{
    class UserValidator : IValidator<User>
    {
        public void Validate(User entity)
        {
            string error = "";

            if (entity.Email == "")
            {
                error += "Email cannot be null";
            }

            if (entity.Password == "")
            {
                error += "Password cannot be null";
            }

            if (error != "")
            {
                throw new ValidatorException(error);
            }
        }
    }
}

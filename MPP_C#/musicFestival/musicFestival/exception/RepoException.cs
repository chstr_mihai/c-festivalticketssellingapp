﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace musicFestival.exception
{
    class RepositoryExceptions : ApplicationException
    {
        public RepositoryExceptions(string message) : base(message)
        {

        }
    }
}

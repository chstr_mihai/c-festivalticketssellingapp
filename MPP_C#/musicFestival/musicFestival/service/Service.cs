﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using musicFestival.repository;
using musicFestival.validators;
using musicFestival.domain;
using musicFestival.exception;

namespace musicFestival.service
{
    class Service
    {
        IShowRepository ShowRepository;
        ITicketRepository TicketRepository;
        IUserRepository UserRepository;
        ShowValidator ShowValidator;
        TicketValidator TicketValidator;
        UserValidator UserValidator;

        public Service(IShowRepository showRepository, ITicketRepository ticketRepository, IUserRepository userRepository, ShowValidator showValidator, TicketValidator ticketValidator, UserValidator userValidator)
        {
            ShowRepository = showRepository;
            TicketRepository = ticketRepository;
            UserRepository = userRepository;
            ShowValidator = showValidator;
            TicketValidator = ticketValidator;
            UserValidator = userValidator;
        }

        public IEnumerable<Show> FindAllShows()
        {
            return ShowRepository.GetAll();
        }

        public IEnumerable<Show> FindShowsOnSpecificDate(DateTime date)
        {

            return ShowRepository.GetShowsOnDate(date);
        }

        private void UpdateSeats(Show show, int dif)
        {

            show.SoldSeats = show.SoldSeats + dif;
            show.AvailableSeats = show.AvailableSeats - dif;

            ShowRepository.Update(show);

        }

        public Ticket SaveTicket(string name, int seats, long idShow)
        {
            Show show = ShowRepository.GetOne(idShow);
            Ticket ticket = new Ticket(name, seats, show);

            if (show.AvailableSeats < seats)
                throw new ServiceException("Not enough available seats!");

            TicketValidator.Validate(ticket);
            Ticket savedTicket = TicketRepository.Save(ticket);
            UpdateSeats(ticket.Show, ticket.SeatsCount);

            return savedTicket;

        }
        public User Login(String email, String password)
        {

            return UserRepository.Login(new User(email, password));

        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace musicFestival.domain
{
    class User : Entity<long>
    {
        public string Email { get; set; }
        public string Password { get; set; }

        public User(long id, string email, string password)
        {
            Id = id;
            Email = email;
            Password = password;
        }

        public User(string email, string password)
        {
            Email = email;
            Password = password;
        }

        public User(long id, string email)
        {
            Id = id;
            Email = email;
        }

        public override string ToString()
        {
            return Id + " " + Email;
        }
    }
}

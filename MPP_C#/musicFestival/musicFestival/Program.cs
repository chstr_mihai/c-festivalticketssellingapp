﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using musicFestival.repository;
using musicFestival.domain;
using musicFestival.service;
using musicFestival.validators;
using System.Windows.Forms;

namespace musicFestival
{
    class Program
    {
        static void Main(string[] args)
        {

            UserRepository userRepository = new UserRepository();
            ShowRepository showRepository = new ShowRepository();
            TicketRepository ticketRepository = new TicketRepository();

            ShowValidator showValidator = new ShowValidator();
            TicketValidator ticketValidator = new TicketValidator();
            UserValidator userValidator = new UserValidator();

            Service service = new Service(showRepository, ticketRepository, userRepository, showValidator, ticketValidator, userValidator);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var login = new LogIn(service);
            Application.Run(login);


        }

    }
}

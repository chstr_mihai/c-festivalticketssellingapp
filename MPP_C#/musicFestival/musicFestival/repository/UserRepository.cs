﻿using musicFestival.domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using System.Data;

namespace musicFestival.repository
{
    class UserRepository : IUserRepository
    {

        private static readonly ILog log = LogManager.GetLogger("UserRepository");

        public UserRepository()
        {
            log.Info("Creating UserRepository");
        }

        public User GetOne(long uid)
        {
          
            log.InfoFormat("Entering getOne with value {0}", uid);
            IDbConnection connection = DBUtils.getConnection();

            User user = null;

            using (var command = connection.CreateCommand())
            {

                command.CommandText = "SELECT * FROM users WHERE uid = @uid";
                IDbDataParameter paramId = command.CreateParameter();
                paramId.ParameterName = "@uid";
                paramId.Value = uid;
                command.Parameters.Add(paramId);

                using (var dataReader = command.ExecuteReader())
                {

                    if (dataReader.Read())
                    {
                    
                        String email = dataReader.GetString(1);
                        user = new User(uid, email);
                
                    }
                
                }
          
            }

            log.InfoFormat("Exiting getOne with value {0}", user);
            return user;

        }

        public IEnumerable<User> GetAll()
        {

            log.InfoFormat("Entering GetAll");
            IDbConnection connection = DBUtils.getConnection();

            List<User> users = new List<User> { };

            using (var command = connection.CreateCommand())
            {

                command.CommandText = "SELECT * FROM users";

                using (var dataReader = command.ExecuteReader())
                {

                    while (dataReader.Read())
                    {

                        long uid = dataReader.GetInt64(0);
                        String email = dataReader.GetString(1);
                        User user = new User(uid, email);
                        users.Add(user);

                    }

                }

            }

            log.InfoFormat("Exiting findOne with value {0}", users);
            return users;

        }

        private bool VerifyEmail(string email, IDbConnection connection)
        {

            log.InfoFormat("Entering verifyEmail with value {0}", email);

            bool response = false;

            using (var command = connection.CreateCommand())
            {

                command.CommandText = "SELECT * FROM users WHERE email = @email";
                IDbDataParameter paramEmail = command.CreateParameter();
                paramEmail.ParameterName = "@email";
                paramEmail.Value = email;
                command.Parameters.Add(paramEmail);

                using (var dataReader = command.ExecuteReader())
                {

                    if (dataReader.Read())
                        response = true;

                }

            }

            log.InfoFormat("Exiting getOne with value {0}", response);
            return response;

        }

        public User Save(User user)
        {

            log.InfoFormat("Entering Save with value {0}", user);
            IDbConnection connection = DBUtils.getConnection();

            if (!VerifyEmail(user.Email, connection))
            {

                using (var command = connection.CreateCommand())
                {

                    command.CommandText = "INSERT INTO users (email, password) VALUES(@email, @password)";

                    IDbDataParameter paramEmail = command.CreateParameter();
                    paramEmail.ParameterName = "@email";
                    paramEmail.Value = user.Email;
                    command.Parameters.Add(paramEmail);

                    IDbDataParameter paramPassword = command.CreateParameter();
                    paramPassword.ParameterName = "@password";
                    paramPassword.Value = user.Password;
                    command.Parameters.Add(paramPassword);

                    command.ExecuteNonQuery();

                }

            }

            log.InfoFormat("Exiting Save with value {0}", user);
            return user;

        }

        public User Delete(long uid)
        {

            log.InfoFormat("Entering Delete with value {0}", uid);
            IDbConnection connection = DBUtils.getConnection();

            User user = GetOne(uid);

            if (user != null)
            {
              
                using (var command = connection.CreateCommand())
                {

                    command.CommandText = "DELETE FROM users WHERE uid = @uid";
                    IDbDataParameter paramId = command.CreateParameter();
                    paramId.ParameterName = "@uid";
                    paramId.Value = uid;
                    command.Parameters.Add(paramId);

                    command.ExecuteNonQuery();

                }
            
            }
            
            log.InfoFormat("Exiting Delete with value {0}", user);
            return user;

        }

        public User Update(User user)
        {

            log.InfoFormat("Entering Update with value {0}", user);
            IDbConnection connection = DBUtils.getConnection();

            using (var command = connection.CreateCommand())
            {

                command.CommandText = "UPDATE users SET email = @email, password = @password WHERE uid = @uid";

                IDbDataParameter paramEmail = command.CreateParameter();
                paramEmail.ParameterName = "@email";
                paramEmail.Value = user.Email;
                command.Parameters.Add(paramEmail);

                IDbDataParameter paramPassword = command.CreateParameter();
                paramPassword.ParameterName = "@password";
                paramPassword.Value = user.Password;
                command.Parameters.Add(paramPassword);

                IDbDataParameter paramId = command.CreateParameter();
                paramId.ParameterName = "@uid";
                paramId.Value = user.Id;
                command.Parameters.Add(paramId);

                if (command.ExecuteNonQuery() == 0)
                    user = null;

            }

            log.InfoFormat("Exiting Update with value {0}", user);
            return user;

        }

        public User Login(User user)
        {

            log.InfoFormat("Entering login with value {0}", user);
            IDbConnection connection = DBUtils.getConnection();

            using (var command = connection.CreateCommand())
            {

                command.CommandText = "SELECT * FROM users WHERE email = @email and password = @password";
                IDbDataParameter paramEmail = command.CreateParameter();
                paramEmail.ParameterName = "@email";
                paramEmail.Value = user.Email;
                command.Parameters.Add(paramEmail);

                IDbDataParameter paramPassword = command.CreateParameter();
                paramPassword.ParameterName = "@password";
                paramPassword.Value = user.Password;
                command.Parameters.Add(paramPassword);

                using (var dataReader = command.ExecuteReader())
                {

                    if (dataReader.Read())
                    {

                        long uid = dataReader.GetInt64(0);
                        user.Id = uid;

                    } 
                    else
                    {
                        user = null;
                    }

                }

            }

            log.InfoFormat("Exiting getOne with value {0}", user);
            return user;

        }
    }
}

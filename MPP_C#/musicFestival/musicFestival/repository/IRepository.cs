﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using musicFestival.domain;

namespace musicFestival.repository
{
    interface IRepository<ID, E> where E : Entity<ID>
    {
        E GetOne(ID id);
        IEnumerable<E> GetAll();
        E Save(E entity);
        E Delete(ID id);
        E Update(E entity);
    }
}

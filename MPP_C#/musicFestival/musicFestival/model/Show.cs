﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace musicFestival.domain
{
    class Show : Entity<long>
    {

        public string Artist { get; set; }
        public DateTime Datetime { get; set; }
        public string Place { get; set; }
        public int AvailableSeats { get; set; }
        public int SoldSeats { get; set; }

        public Show(long id, string artist, DateTime datetime, string place, int availableSeats, int soldSeats)
        {
            Id = id;
            Artist = artist;
            Datetime = datetime;
            Place = place;
            AvailableSeats = availableSeats;
            SoldSeats = soldSeats;
        }

        public Show(string artist, DateTime datetime, string place, int availableSeats, int soldSeats)
        {
            Artist = artist;
            Datetime = datetime;
            Place = place;
            AvailableSeats = availableSeats;
            SoldSeats = soldSeats;
        }

        public override string ToString()
        {
            return Id + ", Artist: " + Artist + ", on " + Datetime.ToString() + ", at " + Place + ", available seats="
                + AvailableSeats + ", sold seats=" + SoldSeats;
        }
    }
}

﻿using musicFestival.domain;
using musicFestival.service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace musicFestival
{
    partial class LogIn : Form
    {
        Service Service;
        public LogIn(Service service)
        {
            Service = service;
            InitializeComponent();
        }


        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void logInBtn_Click(object sender, EventArgs e)
        {
            string email = emailTxtBox.Text;
            string password = passwrodTxtBox.Text;

            try
            {
                User user = Service.Login(email, password);


                this.Hide();
                //Application.EnableVisualStyles();

                //Application.SetCompatibleTextRenderingDefault(false);
                Main main = new Main(Service);
                main.Text = "Welcome!";
                main.Closed += (s, args) => this.Close();
                main.Show();




            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }

        }
    }
}

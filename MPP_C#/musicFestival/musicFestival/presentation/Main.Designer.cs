﻿
namespace musicFestival
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.logOutBtn = new System.Windows.Forms.Button();
            this.tableShows = new System.Windows.Forms.DataGridView();
            this.tableShowsFilteredByDate = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePickerShows = new System.Windows.Forms.DateTimePicker();
            this.searchBtn = new System.Windows.Forms.Button();
            this.displayAllBtn = new System.Windows.Forms.Button();
            this.artistTxtBox = new System.Windows.Forms.TextBox();
            this.seatsTxtBox = new System.Windows.Forms.TextBox();
            this.nameTxtBox = new System.Windows.Forms.TextBox();
            this.locationTxtBox = new System.Windows.Forms.TextBox();
            this.dateTimeTxtBox = new System.Windows.Forms.TextBox();
            this.sellTicketBtn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.tableShows)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableShowsFilteredByDate)).BeginInit();
            this.SuspendLayout();
            // 
            // logOutBtn
            // 
            this.logOutBtn.Location = new System.Drawing.Point(713, 371);
            this.logOutBtn.Name = "logOutBtn";
            this.logOutBtn.Size = new System.Drawing.Size(75, 23);
            this.logOutBtn.TabIndex = 0;
            this.logOutBtn.Text = "Log Out";
            this.logOutBtn.UseVisualStyleBackColor = true;
            this.logOutBtn.Click += new System.EventHandler(this.logOutBtn_Click);
            // 
            // tableShows
            // 
            this.tableShows.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableShows.Location = new System.Drawing.Point(12, 37);
            this.tableShows.Name = "tableShows";
            this.tableShows.Size = new System.Drawing.Size(414, 280);
            this.tableShows.TabIndex = 1;
            this.tableShows.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.tableShows_CellContentClick);
            // 
            // tableShowsFilteredByDate
            // 
            this.tableShowsFilteredByDate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableShowsFilteredByDate.Location = new System.Drawing.Point(12, 37);
            this.tableShowsFilteredByDate.Name = "tableShowsFilteredByDate";
            this.tableShowsFilteredByDate.Size = new System.Drawing.Size(414, 280);
            this.tableShowsFilteredByDate.TabIndex = 2;
            this.tableShowsFilteredByDate.Visible = false;
            this.tableShowsFilteredByDate.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.tableShowsFilteredByDate_CellContentClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(171, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "Shows";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(552, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 25);
            this.label2.TabIndex = 4;
            this.label2.Text = "Info Tickets";
            // 
            // dateTimePickerShows
            // 
            this.dateTimePickerShows.Location = new System.Drawing.Point(226, 349);
            this.dateTimePickerShows.Name = "dateTimePickerShows";
            this.dateTimePickerShows.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerShows.TabIndex = 5;
            // 
            // searchBtn
            // 
            this.searchBtn.Location = new System.Drawing.Point(12, 346);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(75, 23);
            this.searchBtn.TabIndex = 6;
            this.searchBtn.Text = "Search";
            this.searchBtn.UseVisualStyleBackColor = true;
            this.searchBtn.Click += new System.EventHandler(this.searchBtn_Click);
            // 
            // displayAllBtn
            // 
            this.displayAllBtn.Location = new System.Drawing.Point(119, 346);
            this.displayAllBtn.Name = "displayAllBtn";
            this.displayAllBtn.Size = new System.Drawing.Size(75, 23);
            this.displayAllBtn.TabIndex = 7;
            this.displayAllBtn.Text = "Display All";
            this.displayAllBtn.UseVisualStyleBackColor = true;
            this.displayAllBtn.Click += new System.EventHandler(this.displayAllBtn_Click);
            // 
            // artistTxtBox
            // 
            this.artistTxtBox.Location = new System.Drawing.Point(557, 48);
            this.artistTxtBox.Name = "artistTxtBox";
            this.artistTxtBox.Size = new System.Drawing.Size(183, 20);
            this.artistTxtBox.TabIndex = 8;
            // 
            // seatsTxtBox
            // 
            this.seatsTxtBox.Location = new System.Drawing.Point(557, 208);
            this.seatsTxtBox.Name = "seatsTxtBox";
            this.seatsTxtBox.Size = new System.Drawing.Size(183, 20);
            this.seatsTxtBox.TabIndex = 9;
            // 
            // nameTxtBox
            // 
            this.nameTxtBox.Location = new System.Drawing.Point(557, 167);
            this.nameTxtBox.Name = "nameTxtBox";
            this.nameTxtBox.Size = new System.Drawing.Size(183, 20);
            this.nameTxtBox.TabIndex = 10;
            // 
            // locationTxtBox
            // 
            this.locationTxtBox.Location = new System.Drawing.Point(557, 129);
            this.locationTxtBox.Name = "locationTxtBox";
            this.locationTxtBox.Size = new System.Drawing.Size(183, 20);
            this.locationTxtBox.TabIndex = 11;
            // 
            // dateTimeTxtBox
            // 
            this.dateTimeTxtBox.Location = new System.Drawing.Point(557, 90);
            this.dateTimeTxtBox.Name = "dateTimeTxtBox";
            this.dateTimeTxtBox.Size = new System.Drawing.Size(183, 20);
            this.dateTimeTxtBox.TabIndex = 12;
            // 
            // sellTicketBtn
            // 
            this.sellTicketBtn.Location = new System.Drawing.Point(557, 285);
            this.sellTicketBtn.Name = "sellTicketBtn";
            this.sellTicketBtn.Size = new System.Drawing.Size(183, 23);
            this.sellTicketBtn.TabIndex = 13;
            this.sellTicketBtn.Text = "Sell Ticket";
            this.sellTicketBtn.UseVisualStyleBackColor = true;
            this.sellTicketBtn.Click += new System.EventHandler(this.sellTicketBtn_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(469, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 18);
            this.label4.TabIndex = 15;
            this.label4.Text = "Artist:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(447, 92);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 18);
            this.label5.TabIndex = 16;
            this.label5.Text = "Date and Time";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(469, 131);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 18);
            this.label6.TabIndex = 17;
            this.label6.Text = "Location:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(469, 169);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 18);
            this.label7.TabIndex = 18;
            this.label7.Text = "Name:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(469, 210);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 18);
            this.label8.TabIndex = 19;
            this.label8.Text = "Seats:";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.sellTicketBtn);
            this.Controls.Add(this.dateTimeTxtBox);
            this.Controls.Add(this.locationTxtBox);
            this.Controls.Add(this.nameTxtBox);
            this.Controls.Add(this.seatsTxtBox);
            this.Controls.Add(this.artistTxtBox);
            this.Controls.Add(this.displayAllBtn);
            this.Controls.Add(this.searchBtn);
            this.Controls.Add(this.dateTimePickerShows);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tableShowsFilteredByDate);
            this.Controls.Add(this.tableShows);
            this.Controls.Add(this.logOutBtn);
            this.Name = "Main";
            this.Text = "Main";
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tableShows)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableShowsFilteredByDate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button logOutBtn;
        private System.Windows.Forms.DataGridView tableShows;
        private System.Windows.Forms.DataGridView tableShowsFilteredByDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePickerShows;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Button displayAllBtn;
        private System.Windows.Forms.TextBox artistTxtBox;
        private System.Windows.Forms.TextBox seatsTxtBox;
        private System.Windows.Forms.TextBox nameTxtBox;
        private System.Windows.Forms.TextBox locationTxtBox;
        private System.Windows.Forms.TextBox dateTimeTxtBox;
        private System.Windows.Forms.Button sellTicketBtn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}
﻿using log4net;
using musicFestival.domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace musicFestival.repository
{
    class TicketRepository : ITicketRepository
    {

        private static readonly ILog log = LogManager.GetLogger("TicketRepository");

        public Ticket GetOne(long tid)
        {

            log.InfoFormat("Entering getOne with value {0}", tid);
            IDbConnection connection = DBUtils.getConnection();

            Ticket ticket = null;

            using (var command = connection.CreateCommand())
            {

                command.CommandText =
                    "SELECT t.buyer, t.seats_count, s.sid, datetime(s.date_time/1000, 'unixepoch') as date_time, s.place, s.available_seats, s.sold_seats, s.artist FROM tickets t INNER JOIN shows s ON s.sid = t.sid WHERE tid = @tid";
                IDbDataParameter paramId = command.CreateParameter();
                paramId.ParameterName = "@tid";
                paramId.Value = tid;
                command.Parameters.Add(paramId);

                using (var dataReader = command.ExecuteReader())
                {

                    if (dataReader.Read())
                    {

                        long sid = dataReader.GetInt64(2);
                        DateTime dateTime = dataReader.GetDateTime(3);
                        string place = dataReader.GetString(4);
                        int availableSeats = dataReader.GetInt32(5);
                        int soldSeats = dataReader.GetInt32(6);
                        string artist = dataReader.GetString(7);
                        Show show = new Show(sid, artist, dateTime, place, availableSeats, soldSeats);

                        string buyer = dataReader.GetString(0); 
                        int seats_count = dataReader.GetInt32(1);

                        ticket = new Ticket(tid, buyer, seats_count, show);

                    }

                }

            }

            log.InfoFormat("Exiting getOne with value {0}", ticket);
            return ticket;

        }

        public IEnumerable<Ticket> GetAll()
        {

            log.InfoFormat("Entering GetAll");
            IDbConnection connection = DBUtils.getConnection();

            List<Ticket> tickets = new List<Ticket> { };

            using (var command = connection.CreateCommand())
            {

                command.CommandText = "SELECT t.tid, t.buyer, t.seats_count, s.sid, datetime(s.date_time/1000, 'unixepoch') as date_time, s.place, s.available_seats, s.sold_seats, s.artist FROM tickets t INNER JOIN shows s ON s.sid = t.sid";

                using (var dataReader = command.ExecuteReader())
                {

                    while (dataReader.Read())
                    {

                        long sid = dataReader.GetInt64(3);
                        DateTime dateTime = dataReader.GetDateTime(4);
                        string place = dataReader.GetString(5);
                        int availableSeats = dataReader.GetInt32(6);
                        int soldSeats = dataReader.GetInt32(7);
                        string artist = dataReader.GetString(8);
                        Show show = new Show(sid, artist, dateTime, place, availableSeats, soldSeats);

                        long tid = dataReader.GetInt64(0);
                        string buyer = dataReader.GetString(1);
                        int seats_count = dataReader.GetInt32(2);

                        Ticket ticket = new Ticket(tid, buyer, seats_count, show);
                        tickets.Add(ticket);

                    }

                }

            }

            log.InfoFormat("Exiting GetAll");
            return tickets;

        }

        public Ticket Save(Ticket ticket)
        {

            log.InfoFormat("Entering Save with value {0}", ticket);
            IDbConnection connection = DBUtils.getConnection();

            using (var command = connection.CreateCommand())
            {

                command.CommandText =
                    "INSERT INTO tickets (buyer, seats_count, sid) VALUES(@buyer, @seats_count, @sid)";

                IDbDataParameter paramBuyer = command.CreateParameter();
                paramBuyer.ParameterName = "@buyer";
                paramBuyer.Value = ticket.Buyer;
                command.Parameters.Add(paramBuyer);

                IDbDataParameter paramSeatsCount = command.CreateParameter();
                paramSeatsCount.ParameterName = "@seats_count";
                paramSeatsCount.Value = ticket.SeatsCount;
                command.Parameters.Add(paramSeatsCount);

                IDbDataParameter paramSid = command.CreateParameter();
                paramSid.ParameterName = "@sid";
                paramSid.Value = ticket.Show.Id;
                command.Parameters.Add(paramSid);

                command.ExecuteNonQuery();

            }

            log.InfoFormat("Exiting Save with value {0}", ticket);
            return ticket;

        }

        public Ticket Delete(long tid)
        {

            log.InfoFormat("Entering Delete with value {0}", tid);
            IDbConnection connection = DBUtils.getConnection();

            Ticket ticket = GetOne(tid);

            if (ticket != null)
            {

                using (var command = connection.CreateCommand())
                {

                    command.CommandText = "DELETE FROM tickets WHERE tid = @tid";
                    IDbDataParameter paramId = command.CreateParameter();
                    paramId.ParameterName = "@tid";
                    paramId.Value = tid;
                    command.Parameters.Add(paramId);

                    command.ExecuteNonQuery();

                }

            }

            log.InfoFormat("Exiting Delete with value {0}", ticket);
            return ticket;

        }

        public Ticket Update(Ticket ticket)
        {

            log.InfoFormat("Entering Update with value {0}", ticket);
            IDbConnection connection = DBUtils.getConnection();

            using (var command = connection.CreateCommand())
            {

                command.CommandText = "UPDATE tickets SET buyer = @buyer, seats_count = @seats_count, sid = @sid WHERE tid = @tid";

                IDbDataParameter paramBuyer = command.CreateParameter();
                paramBuyer.ParameterName = "@buyer";
                paramBuyer.Value = ticket.Buyer;
                command.Parameters.Add(paramBuyer);

                IDbDataParameter paramSeatsCount = command.CreateParameter();
                paramSeatsCount.ParameterName = "@seats_count";
                paramSeatsCount.Value = ticket.SeatsCount;
                command.Parameters.Add(paramSeatsCount);

                IDbDataParameter paramSid = command.CreateParameter();
                paramSid.ParameterName = "@sid";
                paramSid.Value = ticket.Show.Id;
                command.Parameters.Add(paramSid);

                IDbDataParameter paramId = command.CreateParameter();
                paramId.ParameterName = "@tid";
                paramId.Value = ticket.Id;
                command.Parameters.Add(paramId);

                if (command.ExecuteNonQuery() == 0)
                    ticket = null;

            }

            log.InfoFormat("Exiting Update with value {0}", ticket);
            return ticket;

        }
    }
}

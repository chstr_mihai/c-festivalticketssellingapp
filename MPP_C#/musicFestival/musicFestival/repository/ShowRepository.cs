﻿using musicFestival.domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using log4net;
using System.Data;

namespace musicFestival.repository
{
    class ShowRepository : IShowRepository
    {

        private static readonly ILog log = LogManager.GetLogger("ShowRepository");

        public Show GetOne(long sid)
        {

            log.InfoFormat("Entering getOne with value {0}", sid);
            IDbConnection connection = DBUtils.getConnection();

            Show show = null;

            using (var command = connection.CreateCommand())
            {

                command.CommandText = 
                    "SELECT artist, datetime(date_time/1000, 'unixepoch') as date_time, place, available_seats, sold_seats FROM shows WHERE sid = @sid";
                IDbDataParameter paramId = command.CreateParameter();
                paramId.ParameterName = "@sid";
                paramId.Value = sid;
                command.Parameters.Add(paramId);

                using (var dataReader = command.ExecuteReader())
                {

                    if (dataReader.Read())
                    {

                        string artist = dataReader.GetString(0);
                        DateTime dateTime = dataReader.GetDateTime(1);
                        string place = dataReader.GetString(2);
                        int availableSeats = dataReader.GetInt32(3);
                        int soldSeats = dataReader.GetInt32(4);

                        show = new Show(sid, artist, dateTime, place, availableSeats, soldSeats);

                    }

                }

            }

            log.InfoFormat("Exiting getOne with value {0}", show);
            return show;

        }

        public IEnumerable<Show> GetAll()
        {

            log.InfoFormat("Entering GetAll");
            IDbConnection connection = DBUtils.getConnection();

            List<Show> shows = new List<Show> { };

            using (var command = connection.CreateCommand())
            {

                command.CommandText = "SELECT sid, artist, datetime(date_time/1000, 'unixepoch') as date_time, place, available_seats, sold_seats FROM shows";

                using (var dataReader = command.ExecuteReader())
                {

                    while (dataReader.Read())
                    {

                        long sid = dataReader.GetInt64(0);
                        string artist = dataReader.GetString(1);
                        DateTime dateTime = dataReader.GetDateTime(2);
                        string place = dataReader.GetString(3);
                        int availableSeats = dataReader.GetInt32(4);
                        int soldSeats = dataReader.GetInt32(5);
                        Show show = new Show(sid, artist, dateTime, place, availableSeats, soldSeats);
                        shows.Add(show);

                    }

                }

            }

            log.InfoFormat("Exiting GetAll");
            return shows;

        }

        public Show Save(Show show)
        {
            log.InfoFormat("Entering Save with value {0}", show);
            IDbConnection connection = DBUtils.getConnection();

            using (var command = connection.CreateCommand())
                {

                command.CommandText = 
                    "INSERT INTO shows (artist, date_time, place, available_seats, sold_seats) VALUES(@artist, @datetime, @place, @available_seats, @sold_seats)";

                IDbDataParameter paramArtist = command.CreateParameter();
                paramArtist.ParameterName = "@artist";
                paramArtist.Value = show.Artist;
                command.Parameters.Add(paramArtist);

                IDbDataParameter paramDateTime = command.CreateParameter();
                paramDateTime.ParameterName = "@datetime";
                paramDateTime.Value = show.Datetime;
                command.Parameters.Add(paramDateTime);

                IDbDataParameter paramPlace = command.CreateParameter();
                paramPlace.ParameterName = "@place";
                paramPlace.Value = show.Place;
                command.Parameters.Add(paramPlace);

                IDbDataParameter paramAvailableSeats = command.CreateParameter();
                paramAvailableSeats.ParameterName = "@available_seats";
                paramAvailableSeats.Value = show.AvailableSeats;
                command.Parameters.Add(paramAvailableSeats);

                IDbDataParameter paramSoldSeats = command.CreateParameter();
                paramSoldSeats.ParameterName = "@sold_seats";
                paramSoldSeats.Value = show.SoldSeats;
                command.Parameters.Add(paramSoldSeats);

                command.ExecuteNonQuery();

                }

            log.InfoFormat("Exiting Save with value {0}", show);
            return show;
        }

        public Show Delete(long sid)
        {

            log.InfoFormat("Entering Delete with value {0}", sid);
            IDbConnection connection = DBUtils.getConnection();

            Show show = GetOne(sid);

            if (show != null)
            {

                using (var command = connection.CreateCommand())
                {

                    command.CommandText = "DELETE FROM shows WHERE sid = @sid";
                    IDbDataParameter paramId = command.CreateParameter();
                    paramId.ParameterName = "@sid";
                    paramId.Value = sid;
                    command.Parameters.Add(paramId);

                    command.ExecuteNonQuery();

                }

            }

            log.InfoFormat("Exiting Delete with value {0}", show);
            return show;

        }

        public Show Update(Show show)
        {

            log.InfoFormat("Entering Update with value {0}", show);
            IDbConnection connection = DBUtils.getConnection();

            using (var command = connection.CreateCommand())
            {

                command.CommandText = "UPDATE shows SET artist = @artist, place = @place, available_seats = @available_seats, sold_seats = @sold_seats WHERE sid = @sid";

                IDbDataParameter paramArtist = command.CreateParameter();
                paramArtist.ParameterName = "@artist";
                paramArtist.Value = show.Artist;
                command.Parameters.Add(paramArtist);

                IDbDataParameter paramPlace = command.CreateParameter();
                paramPlace.ParameterName = "@place";
                paramPlace.Value = show.Place;
                command.Parameters.Add(paramPlace);

                IDbDataParameter paramAvailableSeats = command.CreateParameter();
                paramAvailableSeats.ParameterName = "@available_seats";
                paramAvailableSeats.Value = show.AvailableSeats;
                command.Parameters.Add(paramAvailableSeats);

                IDbDataParameter paramSoldSeats = command.CreateParameter();
                paramSoldSeats.ParameterName = "@sold_seats";
                paramSoldSeats.Value = show.SoldSeats;
                command.Parameters.Add(paramSoldSeats);

                IDbDataParameter paramId = command.CreateParameter();
                paramId.ParameterName = "@sid";
                paramId.Value = show.Id;
                command.Parameters.Add(paramId);

                if (command.ExecuteNonQuery() == 0)
                    show = null;

            }

            log.InfoFormat("Exiting Update with value {0}", show);
            return show;

        }

        public IEnumerable<Show> GetShowsOnDate(DateTime date)
        {
            
            log.InfoFormat("Entering GetShowsOnDate with {0}", date);
            IDbConnection connection = DBUtils.getConnection();

            List<Show> shows = new List<Show> { };

            using (var command = connection.CreateCommand())
            {

                command.CommandText =
                    "SELECT sid, artist, date(date_time/1000, 'unixepoch') as date_time, place, available_seats, sold_seats FROM shows WHERE DATETIME(DATE(date_time/1000,'unixepoch')) = @date";

                IDbDataParameter paramDateTime = command.CreateParameter();
                paramDateTime.ParameterName = "@date";
                paramDateTime.Value = date.Date;
                command.Parameters.Add(paramDateTime);

                using (var dataReader = command.ExecuteReader())
                {

                    while (dataReader.Read())
                    {

                        long sid = dataReader.GetInt64(0);
                        string artist = dataReader.GetString(1);
                        DateTime dateTime = dataReader.GetDateTime(2);
                        string place = dataReader.GetString(3);
                        int availableSeats = dataReader.GetInt32(4);
                        int soldSeats = dataReader.GetInt32(5);
                        Show show = new Show(sid, artist, dateTime, place, availableSeats, soldSeats);
                        shows.Add(show);
                        
                    }

                }

                log.InfoFormat("Exiting GetShowsOnDate with {0}", shows);
                return shows;

            }

        }

    }
}

﻿using musicFestival.domain;
using musicFestival.exception;


namespace musicFestival.validators
{
    class TicketValidator
    {

        public void Validate(Ticket entity)
        {
            string error = "";

            if (entity.Buyer == "")
            {
                error += "Customer's name cannot be null";
            }

            if (entity.SeatsCount <= 0)
            {
                error += "Seats bought cannot be less or equal to 0";
            }
            if (error != "")
            {
                throw new ValidatorException(error);
            }
        }

    }
}

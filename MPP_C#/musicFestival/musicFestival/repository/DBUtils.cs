﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SQLite;

namespace musicFestival.repository
{
	public static class DBUtils
	{

		private static IDbConnection instance = null;

		public static IDbConnection getConnection()
		{
			if (instance == null || instance.State == System.Data.ConnectionState.Closed)
			{
				instance = getNewConnection();
				instance.Open();
			}
			return instance;
		}

		private static IDbConnection getNewConnection()
		{
			string conStr = ConfigurationManager.ConnectionStrings["DBO"].ConnectionString;
			SQLiteConnection conn = new SQLiteConnection(conStr);
			return conn;
		}
	}
}

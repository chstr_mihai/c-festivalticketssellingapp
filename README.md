# README #

A C# based desktop application that is designed for music festival ticket sellers. The user interface is based on Windows Forms.

The main functionalities are login (emplyee account), logout, view all shows (with red color the ones that have no more seats 
available), filter shows by date and sell ticket (ticket - buyer's name and seats count; update the list of shows with the new 
available/sold seats left count)
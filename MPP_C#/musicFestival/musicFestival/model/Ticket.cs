﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace musicFestival.domain
{
    class Ticket : Entity<long>
    {
        public string Buyer { get; set; }
        public int SeatsCount { get; set; }

        public Show Show { get; set; }

        public Ticket(long id, string buyer, int seatsCount, Show show)
        {
            Id = id;
            this.Buyer = buyer;
            this.SeatsCount = seatsCount;
            this.Show = show;
        }

        public Ticket(string buyer, int seatsCount, Show show)
        {
            this.Buyer = buyer;
            this.SeatsCount = seatsCount;
            this.Show = show;
        }

        public override string ToString()
        {
            return "Ticket " + Id + ", name: " + Buyer + ", seatsCount: " + SeatsCount + "  -->  Show: " + Show; ;
        }
    }
}
